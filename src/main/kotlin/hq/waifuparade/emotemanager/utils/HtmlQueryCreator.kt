package hq.waifuparade.emotemanager.utils

import hq.waifuparade.emotemanager.enums.PossibleImageTypes
import java.util.*

class HtmlQueryCreator {

    companion object {

        @JvmStatic
        fun createHtmlElementQuery(): String {
            val stringJoiner = StringJoiner(", ")

            for (imageType in PossibleImageTypes.entries) {
                stringJoiner.add("a[href$='.${imageType.name.uppercase()}']")
                stringJoiner.add("a[href$='.${imageType.name.uppercase()}']")
            }

            return stringJoiner.toString()
        }
    }
}