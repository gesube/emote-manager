package hq.waifuparade.emotemanager.utils

import okhttp3.*
import org.springframework.stereotype.Service
import java.util.*

@Service
class HttpConnector {

    private final val connectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
        .tlsVersions(TlsVersion.TLS_1_2)
        .cipherSuites(
            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
        .build()

    final val client = OkHttpClient.Builder()
        .connectionSpecs(Collections.singletonList(connectionSpec))
        .build()

    fun createGetRequest(urlString: String): Request {
        return Request.Builder().url(urlString).build()
    }
}