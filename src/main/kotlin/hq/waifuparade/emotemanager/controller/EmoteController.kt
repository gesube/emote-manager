package hq.waifuparade.emotemanager.controller

import hq.waifuparade.emotemanager.dtos.*
import hq.waifuparade.emotemanager.enums.SearchStringContainsStrategy
import hq.waifuparade.emotemanager.services.EmoteCounter
import hq.waifuparade.emotemanager.services.EmoteDownloader
import hq.waifuparade.emotemanager.services.JsonCreator
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/emotes")
@Tag(name = "emotes", description = "endpoint for managing emotes")
class EmoteController(val emoteDownloader: EmoteDownloader,
                      val emoteCounter: EmoteCounter,
                      val jsonCreator: JsonCreator) {

    @GetMapping("/download")
    fun getJsonForDownloadFunction(): InputDownloadDto {
        val emote = EmoteModelDto(name = "/erklär", image = "https://emotes.waifupara.de/emotes/erklär.png")
        return InputDownloadDto(mutableListOf(emote))
    }

    @GetMapping("/json")
    fun getJsonForCreationFunction(): InputDirLocationDto {
        return InputDirLocationDto(
            remoteSourceDirs = mutableListOf("https://emotes.waifupara.de/emotes/")
        )
    }

    @GetMapping("/count")
    fun getJsonForCountFunction(): InputEmoteCountDto {
        return InputEmoteCountDto(
            searchString = "erklär",
            contains = SearchStringContainsStrategy.CONTAINS_ANYWHERE,
            localSourceDirs = mutableListOf("./emotes"),
            remoteSourceDirs = mutableListOf("https://emotes.waifupara.de/emotes/")
        )
    }

    @PostMapping("/download")
    fun downloadEmotes(@RequestBody inputDto: InputDownloadDto) {
        this.emoteDownloader.downloadEmotes(inputDto)
    }

    @PostMapping("/json")
    fun createJsonForEmotes(@RequestBody inputDto: InputDirLocationDto): MutableCollection<EmoteModelDto> {
        return this.jsonCreator.createJson(inputDto)
    }

    @PostMapping("/count")
    fun countEmotes(@RequestBody inputDto: InputEmoteCountDto): OutputEmoteCountDto {
        return this.emoteCounter.countEmotes(inputDto)
    }
}