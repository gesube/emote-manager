package hq.waifuparade.emotemanager

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.core.env.AbstractEnvironment
import org.springframework.core.env.EnumerablePropertySource
import org.springframework.core.io.support.ResourcePropertySource
import org.springframework.stereotype.Component
import java.util.*
import java.util.stream.StreamSupport

@Component
class StartupPropertyLogger {

    private val logger = KotlinLogging.logger {}

    /**
     * Die Methode sorgt dafür, dass beim Starten der Anwendung
     * alle angegebenen Daten der application.properties in der Konsole ausgegeben werden.
     */
    @EventListener
    fun handleContextRefresh(event: ContextRefreshedEvent) {
        val env = event.applicationContext.environment
        logger.info {"====== Environment and configuration ======"}
        val sources = (env as AbstractEnvironment).propertySources

        StreamSupport.stream(sources.spliterator(), false)
            .filter { propertySource -> propertySource is ResourcePropertySource }
            .map { propertySource -> (propertySource as EnumerablePropertySource<*>).propertyNames }
            .flatMap(Arrays::stream)
            .sorted()
            .forEach { property -> logger.info {"$property: ${env.getProperty(property)}"}  }

        logger.info {"==========================================="}
    }

}