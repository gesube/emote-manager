package hq.waifuparade.emotemanager

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.PropertySource

@SpringBootApplication
@PropertySource("application.properties", ignoreResourceNotFound = true)
class EmoteManagerApplication {

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			runApplication<EmoteManagerApplication>(*args)
		}
	}
}