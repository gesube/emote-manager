package hq.waifuparade.emotemanager.dtos

class InputDownloadDto(
    val emoteList: MutableList<EmoteModelDto> = mutableListOf(),
    val targetDir: String = "./emotes"
)