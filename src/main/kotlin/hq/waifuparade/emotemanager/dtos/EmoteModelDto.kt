package hq.waifuparade.emotemanager.dtos

class EmoteModelDto(
    val name: String = "",
    val image: String = ""
)