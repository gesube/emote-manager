package hq.waifuparade.emotemanager.dtos

import hq.waifuparade.emotemanager.enums.SearchStringContainsStrategy

class InputEmoteCountDto(
    val searchString: String = "",
    val contains: SearchStringContainsStrategy = SearchStringContainsStrategy.CONTAINS_ANYWHERE,
    val localSourceDirs: List<String> = listOf(),
    val remoteSourceDirs: List<String> = listOf(),
    val ignoreCase: Boolean = true
)