package hq.waifuparade.emotemanager.dtos

import hq.waifuparade.emotemanager.enums.SearchStringContainsStrategy

class OutputEmoteCountDto(
    val searchString: String,
    val ignoreCase: Boolean,
    val contains: SearchStringContainsStrategy,
    val numberOfMatchingEmotes: Int,
    val listOfMatchingEmoteNames: MutableCollection<String>
)