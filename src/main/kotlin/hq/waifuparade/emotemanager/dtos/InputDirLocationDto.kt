package hq.waifuparade.emotemanager.dtos

class InputDirLocationDto(
    val remoteSourceDirs: List<String> = mutableListOf(),
    val localSourceDirs: List<String> = mutableListOf()
)