package hq.waifuparade.emotemanager.services

import hq.waifuparade.emotemanager.dtos.EmoteModelDto
import hq.waifuparade.emotemanager.dtos.InputDirLocationDto
import hq.waifuparade.emotemanager.enums.PossibleImageTypes
import hq.waifuparade.emotemanager.utils.HtmlQueryCreator
import hq.waifuparade.emotemanager.utils.HttpConnector
import org.jsoup.Jsoup
import org.springframework.stereotype.Service
import java.io.File
import java.net.URLDecoder

@Service
class JsonCreator(private val httpConnector: HttpConnector) {

    private val emoteNameProperty = "emoteName"
    private val emoteExtensionProperty = "emoteExtension"

    fun createJson(inputDto: InputDirLocationDto): MutableCollection<EmoteModelDto> {
        val resultMap: MutableMap<String, EmoteModelDto> = mutableMapOf()
        this.createJsonForRemoteDirectories(inputDto = inputDto, resultMap = resultMap)
        this.createJsonForLocalDirectories(inputDto = inputDto, resultMap = resultMap)

        return resultMap.values
    }

    private fun createJsonForRemoteDirectories(inputDto: InputDirLocationDto, resultMap: MutableMap<String, EmoteModelDto>) {
        inputDto.remoteSourceDirs.forEach { remoteSourceDir ->
            val request = this.httpConnector.createGetRequest(remoteSourceDir)

            this.httpConnector.client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    println("$remoteSourceDir ist nicht erreichbar und wird daher bei der Json_Erstellung nicht berücksichtigt.")
                } else {
                    val htmlContent = response.body?.source()?.readUtf8()

                    htmlContent?.let {
                        val doc = Jsoup.parse(it)
                        val imageLinks = doc.select(HtmlQueryCreator.createHtmlElementQuery())

                        for (imageLink in imageLinks) {
                            // Der Query filtert bereits alle anchor-Elemente ohne href weg
                            // Hier kann man sich daher darauf verlassen, dass das Attribut existiert
                            val imageName = imageLink.attributes()["href"]
                            val decodedImageName = URLDecoder.decode(imageName, "UTF-8")
                            val emoteProperties = this.filterImageProperties(decodedImageName)
                            val emoteModel = this.createEmoteModel(
                                directory = remoteSourceDir,
                                emoteName = emoteProperties[emoteNameProperty] as String,
                                fileExtension = emoteProperties[emoteExtensionProperty] as String
                            )

                            resultMap[emoteProperties[emoteNameProperty] as String] = emoteModel
                        }
                    }
                }
            }
        }
    }

    private fun createJsonForLocalDirectories(inputDto: InputDirLocationDto, resultMap: MutableMap<String, EmoteModelDto>) {
        inputDto.localSourceDirs.forEach { localSourceDir ->
            val sourceDir = File(localSourceDir)
            val sourceFiles = sourceDir.listFiles()
            val remoteDirectory = this.getRemoteDirStringFromLocalDirString(localDirectory = localSourceDir)

            sourceFiles?.forEach { file ->
                val emoteProperties = this.filterImageProperties(file.name)

                val emoteModel = this.createEmoteModel(
                    directory = remoteDirectory,
                    emoteName = emoteProperties[emoteNameProperty] as String,
                    fileExtension = emoteProperties[emoteExtensionProperty] as String
                )

                resultMap[emoteProperties[emoteNameProperty] as String] = emoteModel
            }
        }
    }

    private fun filterImageProperties(imageName: String): Map<String, String> {
        val imagePropertiesMap = mutableMapOf<String, String>()

        for (imageType in PossibleImageTypes.entries) {
            if (imageName.endsWith(imageType.name, true)) {
                // index errechnet sich aus Gesamtlänge - (Zeichenanzahl der Endung + den Punkt vor der Endung)
                val indexOfLastChar = imageName.length - (imageType.name.length+1)
                imagePropertiesMap[emoteNameProperty] = imageName.substring(0, indexOfLastChar)
                imagePropertiesMap[emoteExtensionProperty] = imageType.name.lowercase()
            }
        }

        return imagePropertiesMap
    }

    private fun getRemoteDirStringFromLocalDirString(localDirectory: String): String {
        val index = localDirectory.indexOf("emotes-special")
        val remoteDir = if (index < 0) {
            "emotes"
        } else {
            localDirectory.substring(index, localDirectory.length)
        }

        return remoteDir.replace("\\", "/")
    }

    private fun createEmoteModel(directory: String, emoteName: String, fileExtension: String): EmoteModelDto {
        var remoteDirectory = if (directory.startsWith("http")) {
            // Wenn ein remote-Verzeichnis übergeben wurde, soll das übernommen werden
            directory
        } else {
            // ansonsten muss es noch zusammengebaut werden
            "https://emotes.waifupara.de/$directory"
        }

        // Vereinheitlichung des Verzeichnisnamens
        if (remoteDirectory.endsWith("/")) {
            remoteDirectory = remoteDirectory.substring(0, remoteDirectory.length-1)
        }

        return EmoteModelDto(
            name = "/$emoteName",
            image = "$remoteDirectory/$emoteName.$fileExtension"
        )
    }
}