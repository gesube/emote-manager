package hq.waifuparade.emotemanager.services

import hq.waifuparade.emotemanager.dtos.InputEmoteCountDto
import hq.waifuparade.emotemanager.dtos.OutputEmoteCountDto
import hq.waifuparade.emotemanager.enums.PossibleImageTypes
import hq.waifuparade.emotemanager.enums.SearchStringContainsStrategy
import hq.waifuparade.emotemanager.utils.HtmlQueryCreator
import hq.waifuparade.emotemanager.utils.HttpConnector
import org.jsoup.Jsoup
import org.springframework.stereotype.Service
import java.io.File
import java.net.URLDecoder

@Service
class EmoteCounter(private val httpConnector: HttpConnector) {

    fun countEmotes(inputDto: InputEmoteCountDto): OutputEmoteCountDto {
        val resultMap: MutableMap<String, String> = mutableMapOf()

        this.countEmotesFromRemoteDirectories(inputDto = inputDto, resultMap = resultMap)
        this.countEmotesFromLocalDirectories(inputDto = inputDto, resultMap = resultMap)

        return OutputEmoteCountDto(
            searchString = inputDto.searchString,
            ignoreCase = inputDto.ignoreCase,
            contains = inputDto.contains,
            numberOfMatchingEmotes = resultMap.size,
            listOfMatchingEmoteNames = resultMap.values
        )
    }

    private fun countEmotesFromRemoteDirectories(inputDto: InputEmoteCountDto, resultMap: MutableMap<String, String>) {
        inputDto.remoteSourceDirs.forEach { remoteSourceDir ->
            val request = this.httpConnector.createGetRequest(remoteSourceDir)

            this.httpConnector.client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    println("$remoteSourceDir ist nicht erreichbar und wird daher beim Zählen nicht berücksichtigt.")
                } else {
                    val htmlContent = response.body?.source()?.readUtf8()

                    htmlContent?.let {
                        val doc = Jsoup.parse(it)
                        val imageLinks = doc.select(HtmlQueryCreator.createHtmlElementQuery())

                        for (imageLink in imageLinks) {
                            // Der Query filtert bereits alle anchor-Elemente ohne href weg
                            // Hier kann man sich daher darauf verlassen, dass das Attribut existiert
                            val imageName = imageLink.attributes()["href"]
                            val decodedImageName = URLDecoder.decode(imageName, "UTF-8")

                            val doesMatch = this.checkIfEmoteNameContainsSearchString(
                                searchString = inputDto.searchString,
                                searchStrategy = inputDto.contains,
                                emoteName = decodedImageName,
                                ignoreCase = inputDto.ignoreCase
                            )

                            if (doesMatch) {
                                resultMap[decodedImageName] = decodedImageName
                            }
                        }
                    }
                }
            }
        }
    }

    private fun countEmotesFromLocalDirectories(inputDto: InputEmoteCountDto, resultMap: MutableMap<String, String>) {
        inputDto.localSourceDirs.forEach { localSourceDir ->
            val sourceDir = File(localSourceDir)
            val sourceFiles = sourceDir.listFiles()

            sourceFiles?.forEach { file ->
                val doesMatch = this.checkIfEmoteNameContainsSearchString(
                    searchString = inputDto.searchString,
                    searchStrategy = inputDto.contains,
                    emoteName = file.name,
                    ignoreCase = inputDto.ignoreCase
                )

                if (doesMatch) {
                    resultMap[file.name] = file.name
                }
            }
        }
    }

    private fun checkIfEmoteNameContainsSearchString(
        searchString: String, searchStrategy: SearchStringContainsStrategy, emoteName: String, ignoreCase: Boolean): Boolean {
        var filteredEmoteName = emoteName

        for (imageType in PossibleImageTypes.entries) {
            if (emoteName.endsWith(imageType.name, true)) {
                // index errechnet sich aus Gesamtlänge - (Zeichenanzahl der Endung + den Punkt vor der Endung)
                val indexOfLastChar = emoteName.length - (imageType.name.length+1)
                filteredEmoteName = emoteName.substring(0, indexOfLastChar)
            }
        }

        return when (searchStrategy) {
            SearchStringContainsStrategy.CONTAINS_AT_BEGINNING -> { filteredEmoteName.startsWith(searchString, ignoreCase) }
            SearchStringContainsStrategy.CONTAINS_AT_END -> { filteredEmoteName.endsWith(searchString, ignoreCase) }
            else -> { filteredEmoteName.contains(searchString, ignoreCase) }
        }
    }
}