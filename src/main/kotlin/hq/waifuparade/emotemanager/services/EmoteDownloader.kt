package hq.waifuparade.emotemanager.services

import hq.waifuparade.emotemanager.dtos.InputDownloadDto
import hq.waifuparade.emotemanager.utils.HttpConnector
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream

@Service
class EmoteDownloader(private val httpConnector: HttpConnector) {

    fun downloadEmotes(inputDto: InputDownloadDto) {
        this.createTargetDirIfNeeded(inputDto.targetDir)

        inputDto.emoteList.forEach { emoteModel ->
            val request = this.httpConnector.createGetRequest(emoteModel.image)

            this.httpConnector.client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    println("${emoteModel.image} ist nicht erreichbar und wird daher nicht heruntergeladen.")
                } else {
                    // Erstelle die Datei...
                    val fileContent = response.body?.bytes() as ByteArray
                    val fileName = this.sanitizeFileName(emoteModel.name)
                    val fileExtension = response.body?.contentType()?.subtype
                    val file = File("${inputDto.targetDir}/${fileName}.${fileExtension}")

                    // ...auf der Festplatte
                    val fileOutputStream = FileOutputStream(file)
                    fileOutputStream.write(fileContent)
                    fileOutputStream.close()
                }
            }
        }
    }

    private fun createTargetDirIfNeeded(targetDirAsString: String) {
        val targetDir = File(targetDirAsString)
        if (!targetDir.exists()) {
            this.createTargetDirIfNeeded(targetDir.parent)
            targetDir.mkdir()
        }
    }

    private fun sanitizeFileName(fileName: String): String {
        // sorgt aktuell nur dafür, dass die '/'s am Anfang entfernt werden
        return fileName.substring(1)
    }
}