package hq.waifuparade.emotemanager

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


/**
 * Klasse, in der die Konfiguration von Swagger gekapselt ist.
 */
@Configuration
class SwaggerConfig {

    @Bean
    fun customOpenAPI(): OpenAPI {
        return OpenAPI().info(Info().title("Emote Manager API").version("1.0.0"))
    }
}