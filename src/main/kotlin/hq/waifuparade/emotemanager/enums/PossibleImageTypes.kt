package hq.waifuparade.emotemanager.enums

enum class PossibleImageTypes {

    JPG,
    PNG,
    GIF,
    WEBP

}