package hq.waifuparade.emotemanager.enums

enum class SearchStringContainsStrategy {

    CONTAINS_AT_BEGINNING,
    CONTAINS_AT_END,
    CONTAINS_ANYWHERE

}